import numpy as np
image_names = np.load("./image_names.npy").tolist()
new_names = []
for n in image_names:
    if 'warp' in n:
        new_names.append(n)
        #保存图像名称，以便索引
image_names = np.array(new_names)
print(len(image_names))
print(image_names)
np.save('image_names_new.npy', image_names)