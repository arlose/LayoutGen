# coding=utf-8
import random
import os

infilename = "content/en/TFAoRC.txt.bak"
outfilename = "content/en/TFAoRC.txt"


seq = []
with open(infilename, "r") as f:
    lines = f.readlines()
    cl = ""
    count = 0
    blknum = random.randint(20,40)
    for c in lines[0]:
        cl = cl+c
        if c == ' ':
            count = count+1
        if count>blknum:
            count = 0
            blknum = random.randint(20,40)
            seq.append(cl+'\n')
            # print("##################################", cl)
            cl = ""

print(len(seq))

fo = open(outfilename, "w")
fo.writelines(seq)
fo.close()
