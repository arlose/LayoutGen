'''
Desc: 
Author: KouXichao
Date: 2020-12-25 14:48:47
Contact: kouxichao@qq.com
LastEditors: KouXichao
LastEditTime: 2020-12-25 16:20:28
FilePath: /LayoutGen/tools/genLaTeX.py
Github: https://github.com/kouxichao
-----------------------------------------------------------------------------
FreeType high-level python API - Copyright 2020-2020 Nicolas P. Rougier
Distributed under the terms of the new BSD license.
-----------------------------------------------------------------------------

'''

# import math
# import latexify

# @latexify.with_latex
# def solve(a, b, c):
#   return (-b + math.sqrt(b**2 - 4*a*c)) / (2*a)

# print(solve(1, 4, 3))
# print(solve)
# print()
# solve
from matplotlib import rcParams
rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
from io import BytesIO
import matplotlib.pyplot as plt
def renderlatex(formula, fontsize=18, dpi=300, format_='png'):
    """Renders LaTeX formula into image.
    """
    fig = plt.figure(figsize=(0.01, 0.01))
    fig.text(0, 0, u'${}'.format(formula), fontsize=fontsize)
    buffer_ = io.BytesIO()
    fig.savefig(buffer_, dpi=dpi, transparent=True, format=format_, bbox_inches='tight', pad_inches=0.0)
    return buffer_.getvalue()

import PIL.Image as img
b=BytesIO()
b.write(renderlatex('\frac{1}{3}'))
b.seek(0)
i=img.open(b)
(h,w)=i.size