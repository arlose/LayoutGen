#!/bin/bash

read -p "请输入压缩格式(zip or tar, default:zip): " compressMeth

compressMeth=${compressMeth:-"zip"}

echo "**********************************************"
echo -e "\033[31mstart compress SynthLayout data\033[0m"
echo "**********************************************"
if  test $compressMeth = "zip"; then
    ${compressMeth} -r  SynthLayoutData.zip ./synthetic_data/image ./synthetic_data/regionInfo ./synthetic_data/image_names.npy
    ${compressMeth} -r  SynthlineData.zip ./synthetic_data/line_image ./synthetic_data/gt.txt
elif test $compressMeth = "tar"; then
    ${compressMeth} -czvf  SynthLayoutData.tar.gz ./synthetic_data/image ./synthetic_data/regionInfo ./synthetic_data/image_names.npy
    ${compressMeth} -czvf  SynthlineData.tar.gz ./synthetic_data/line_image ./synthetic_data/gt.txt
else
    echo -e "only support zip and tar !!!"
fi

echo "**********************************************"
echo -e "\033[31m!!!!!!!!!!!!!!!END!!!!!!!!!!!!!\033[0m"
echo "**********************************************"