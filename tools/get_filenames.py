'''
Desc: 
Author: KouXichao
Date: 2021-01-02 19:08:29
Contact: kouxichao@qq.com
LastEditors: KouXichao
LastEditTime: 2021-01-02 19:21:47
FilePath: /LayoutGen/tools/get_filenames.py
Github: https://github.com/kouxichao
-----------------------------------------------------------------------------
FreeType high-level python API - Copyright 2020-2020 Nicolas P. Rougier
Distributed under the terms of the new BSD license.
-----------------------------------------------------------------------------

'''

import os
import argparse

parser = argparse.ArgumentParser(description="save filenames in txt")
parser.add_argument('-fp', "--filepath", required=True, type=str, help="Path of file")
parser.add_argument('-sn', "--save_name", default="train.txt", type=str, help="txt of filenames")
parser.add_argument("-fe", '--file_ext', default="jpg", type=str, help="files' extention")
opt = parser.parse_args()

with open(os.path.join(opt.filepath, '../' + opt.save_name), 'w+') as f:
    for _, _, filenames in os.walk(opt.filepath):
        for filename in filenames:
            if filename.endswith(opt.file_ext):
                f.write(filename.split('.')[0] + '\n')
