# coding=utf-8
background = 'white'

fontPath = './font/'
outFileName = 'sample.jpg'

color_RGB = {
    'black' : [0, 0, 0],
    'ivory black' : [41, 36, 33],
    'gray' : [192, 192, 192],
    'cold gray' : [128, 138, 135],
    'slate gray' : [112, 128, 105],
    'warm gray' : [128, 128, 105],
    'yellow' : [255, 255, 0],
    'banana' : [227, 207, 87],
    'cadmium yellow' : [255, 153, 18],
    'brown' : [128, 42, 42],
    'beige' : [163, 148, 128],
    'red' : [255, 0, 0],
    'poppy' : [176, 48, 96],
    'pink' : [255, 192, 203],
    'blue' : [0, 0, 255],
    'peacock blue' : [51, 161, 201],
    'cyan' : [0, 255, 255],
    'indigo' : [8, 46, 84],
    'green' : [0, 255, 0],
    'purple' : [160, 32, 240],
    'violet' : [138, 43, 226],
    'plum red' : [221, 160, 221]
}

picWidth = 1080
picHeight = 1920
regionList = [{'attr': 'title', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region0', 'x': 225, 'y': 255, 'h': 165, 'w': 692, 'dir': 'h'}, {'attr': 'title', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region1', 'x': 329, 'y': 594, 'h': 145, 'w': 697, 'dir': 'h'}, {'attr': 'title', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region2', 'x': 332, 'y': 1255, 'h': 127, 'w': 747, 'dir': 'h'}, {'attr': 'title', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region3', 'x': 305, 'y': 1411, 'h': 155, 'w': 555, 'dir': 'h'}, {'attr': 'formula', 'name': 'region4', 'x': 200, 'y': 38, 'h': 67, 'w': 842}, {'attr': 'formula', 'name': 'region5', 'x': 196, 'y': 815, 'h': 90, 'w': 682}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region6', 'x': 16, 'y': 120, 'h': 123, 'w': 1047, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region7', 'x': 17, 'y': 433, 'h': 141, 'w': 1045, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region8', 'x': 20, 'y': 750, 'h': 50, 'w': 1038, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region9', 'x': 11, 'y': 924, 'h': 317, 'w': 1056, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region10', 'x': 8, 'y': 1584, 'h': 321, 'w': 1063, 'dir': 'h'}]
numRegion = 11
