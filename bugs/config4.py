# coding=utf-8
background = 'white'

fontPath = './font/'
outFileName = 'sample.jpg'

color_RGB = {
    'black' : [0, 0, 0],
    'ivory black' : [41, 36, 33],
    'gray' : [192, 192, 192],
    'cold gray' : [128, 138, 135],
    'slate gray' : [112, 128, 105],
    'warm gray' : [128, 128, 105],
    'yellow' : [255, 255, 0],
    'banana' : [227, 207, 87],
    'cadmium yellow' : [255, 153, 18],
    'brown' : [128, 42, 42],
    'beige' : [163, 148, 128],
    'red' : [255, 0, 0],
    'poppy' : [176, 48, 96],
    'pink' : [255, 192, 203],
    'blue' : [0, 0, 255],
    'peacock blue' : [51, 161, 201],
    'cyan' : [0, 255, 255],
    'indigo' : [8, 46, 84],
    'green' : [0, 255, 0],
    'purple' : [160, 32, 240],
    'violet' : [138, 43, 226],
    'plum red' : [221, 160, 221]
}

picWidth = 1080
picHeight = 1920
regionList = [{'attr': 'title', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region0', 'x': 339, 'y': 378, 'h': 155, 'w': 716, 'dir': 'h'}, {'attr': 'title', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region1', 'x': 87, 'y': 772, 'h': 179, 'w': 676, 'dir': 'h'}, {'attr': 'title', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region2', 'x': 182, 'y': 1131, 'h': 141, 'w': 822, 'dir': 'h'}, {'attr': 'title', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region3', 'x': 77, 'y': 1454, 'h': 134, 'w': 936, 'dir': 'h'}, {'attr': 'formula', 'name': 'region4', 'x': 10, 'y': 1281, 'h': 82, 'w': 534}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region5', 'x': 5, 'y': 35, 'h': 329, 'w': 528, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region6', 'x': 552, 'y': 35, 'h': 329, 'w': 520, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region7', 'x': 20, 'y': 550, 'h': 202, 'w': 499, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region8', 'x': 552, 'y': 550, 'h': 202, 'w': 511, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region9', 'x': 17, 'y': 966, 'h': 148, 'w': 505, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region10', 'x': 553, 'y': 966, 'h': 148, 'w': 516, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region11', 'x': 14, 'y': 1378, 'h': 58, 'w': 510, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region12', 'x': 557, 'y': 1286, 'h': 149, 'w': 513, 'dir': 'h'}, {'attr': 'text', 'blank': 20, 'lan': 'en', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region13', 'x': 13, 'y': 1602, 'h': 301, 'w': 512, 'dir': 'h'}, {'attr': 'image', 'name': 'region14', 'x': 560, 'y': 1602, 'h': 301, 'w': 510}]
numRegion = 15
