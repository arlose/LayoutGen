# coding=utf-8
background = 'white'

fontPath = './font/'
outFileName = 'sample.jpg'

color_RGB = {
    'black' : [0, 0, 0],
    'ivory black' : [41, 36, 33],
    'gray' : [192, 192, 192],
    'cold gray' : [128, 138, 135],
    'slate gray' : [112, 128, 105],
    'warm gray' : [128, 128, 105],
    'yellow' : [255, 255, 0],
    'banana' : [227, 207, 87],
    'cadmium yellow' : [255, 153, 18],
    'brown' : [128, 42, 42],
    'beige' : [163, 148, 128],
    'red' : [255, 0, 0],
    'poppy' : [176, 48, 96],
    'pink' : [255, 192, 203],
    'blue' : [0, 0, 255],
    'peacock blue' : [51, 161, 201],
    'cyan' : [0, 255, 255],
    'indigo' : [8, 46, 84],
    'green' : [0, 255, 0],
    'purple' : [160, 32, 240],
    'violet' : [138, 43, 226],
    'plum red' : [221, 160, 221]
}

picWidth = 1080
picHeight = 1920
regionList = [{'attr': 'title', 'blank': 20, 'lan': 'cn', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region0', 'x': 237, 'y': 865, 'h': 152, 'w': 834, 'dir': 'h'}, {'attr': 'formula', 'name': 'region1', 'x': 285, 'y': 262, 'h': 83, 'w': 564}, {'attr': 'formula', 'name': 'region2', 'x': 136, 'y': 1085, 'h': 74, 'w': 640}, {'attr': 'text', 'blank': 20, 'lan': 'cn', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region3', 'x': 7, 'y': 32, 'h': 212, 'w': 1065, 'dir': 'v'}, {'attr': 'text', 'blank': 20, 'lan': 'cn', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region4', 'x': 11, 'y': 356, 'h': 494, 'w': 1057, 'dir': 'v'}, {'attr': 'text', 'blank': 20, 'lan': 'cn', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region5', 'x': 19, 'y': 1035, 'h': 31, 'w': 1041, 'dir': 'v'}, {'attr': 'text', 'blank': 20, 'lan': 'cn', 'lineSpacing': 0, 'paraSpacing': 0, 'name': 'region6', 'x': 9, 'y': 1176, 'h': 725, 'w': 1060, 'dir': 'v'}]
numRegion = 7
