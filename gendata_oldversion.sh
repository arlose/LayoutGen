#!/bin/bash

read -p "请输入生成图片数量(default:100): " num
read -p "请输入保存XML和json文件的路径(default:./synthetic_data/xml/): " path
read -p "请输入要生成的语言(["en", "jp", "cn"], default:en): " language
read -p "是否生成扭曲图像([t or f], default:f): " addWarp
read -p "选择需要插入的图像类型([0:image_word(normal_image), 1:image_with_textGT, 2:image_without_word], default:0): " image_type
read -p "请输入最后输出的json文件的名字（如 test, default:jsondata）: " name
read -p "是否重新生成数据集(清除原有数据)([t or f], default:f)": var1

# echo $var1
num=${num:-100}
path=${path:-"./synthetic_data/xml/"}
name=${name:-"jsondata"}
var1=${var1:-"f"}
addWarp_=${addWarp:-"f"}
image_type=${image_type:-0}
language=${language:-"en"}

echo "--------------CONFIG INFO------------------"
        echo "num: ${num}"
        echo "path: ${path}"
        echo "addWarp: ${addWarp_}"
        echo "region_image_type: ${image_type}"
        echo "jsondata: ${name}"
        echo "remove images and gts: ${var1}"
echo "-------------CONFIG INFO-------------------"
echo " "

if  test $addWarp_ = "t"; then
        addWarp_="--addWarp"
else
        addWarp_=""
fi

if  test $var1 = "t"; then 
        echo "remove imagedata"
        rm -r ./synthetic_data/*

        
        # ocr train data
        # rm -r ./synthetic_data/line_images/*
        # rm -r ./synthetic_data/gt.txt

        # layout images and region info images
        rm -r ./synthetic_data/procfile_image/*
fi

for i in  `seq $num`;do
        echo "\033[31m**********************************************"
        echo -e "start to generate "$i"th image"
        echo "**********************************************\033[0m"
        # python randomlayout_onlyTextImage.py
        python randomlayout.py --language ${language}
        python main.py ${addWarp_} --region_image_type ${image_type}
        python create_xml.py --xmlPath ${path} 
        python data2labelme_json.py --jsonPath ${path}
done

echo "开始整合全部的json"
python labelme2coco.py --jsonPath ${path} --jsonName ${name}